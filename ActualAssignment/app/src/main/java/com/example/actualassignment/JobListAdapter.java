package com.example.actualassignment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class JobListAdapter extends BaseAdapter
{

    Context jobContext;
    ArrayList< Job > jobs;
    Job singleJob;

    public JobListAdapter( Context c, ArrayList< Job > jobList )
    {
        this.jobContext = c;
        this.jobs = jobList;
    }

    @Override
    public int getCount() {
        return jobs.size();
    }

    @Override
    public Object getItem( int position )
    {
        return jobs.get( position );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if ( convertView == null )
        {
            LayoutInflater jobInflater = (LayoutInflater) this.jobContext.getSystemService(jobContext.LAYOUT_INFLATER_SERVICE);

            convertView = jobInflater.inflate(R.layout.adapter_view_layout, null);
        }

        // Get current displayed item.
        singleJob = jobs.get(position);

        // If there actually is an item.
        if (singleJob!=null)
        {
            // Getting the jobtitle-textview from the layout.
            TextView titleOfJob = (TextView)convertView.findViewById(R.id.jobTitleJobXML);
            // Sets the newly-found textview to the title of the job-object.
            titleOfJob.setText(singleJob.getJobTitle());


            TextView descriptionOfJob = (TextView)convertView.findViewById(R.id.jobCompany);
            descriptionOfJob.setText(singleJob.getJobCompany());

            TextView scoreOfJob = (TextView)convertView.findViewById(R.id.jobScore);
            scoreOfJob.setText(String.valueOf(singleJob.getJobScore()));

            TextView isItApplied = (TextView)convertView.findViewById(R.id.appliedOrNot);
            if (singleJob.isAppliedOrNot())
            {
                isItApplied.setText("Applied");
            }
            else
            {
                isItApplied.setText("Not applied");
            }

            ImageView jobLogo = (ImageView)convertView.findViewById(R.id.jobImage);
            jobLogo.setImageResource(singleJob.getJobImageResourceId());
        }


        return convertView;
    }
}