package com.example.actualassignment;

import android.widget.ImageView;

import java.io.Serializable;

public class Job implements Serializable {
    private ImageView JobImage;
    private int ImageResourceId = 0;
    private String JobTitle;
    private String JobCompany;
    private String Description;
    private String Location;
    private int JobScore;

    public String getJobNotes() {
        return JobNotes;
    }

    public void setJobNotes(String jobNotes) {
        JobNotes = jobNotes;
    }

    private String JobNotes;

    public int getImageResourceId() {
        return ImageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        ImageResourceId = imageResourceId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    private boolean AppliedOrNot;

    public Job(String jobcompany, String jobtitle, int jobScore, boolean appliedOrNot, String description, String location) {
        this.JobCompany = jobcompany;
        this.JobTitle = jobtitle;
        this.JobScore = jobScore;
        this.AppliedOrNot = appliedOrNot;
        this.Description = description;
        this.Location = location;
    }

    public Job(String jobcompany, String jobtitle, int jobScore, boolean appliedOrNot, String description, String location, ImageView companyimage) {
        this.JobCompany = jobcompany;
        this.JobTitle = jobtitle;
        this.JobScore = jobScore;
        this.AppliedOrNot = appliedOrNot;
        this.Description = description;
        this.Location = location;
        this.JobImage = companyimage;
    }

    public String getJobTitle() {
        return JobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.JobTitle = jobTitle;
    }

    public String getJobCompany() {
        return JobCompany;
    }

    public void setJobCompany(String jobcompany) {
        this.JobCompany = jobcompany;
    }

    public int getJobScore() {
        return JobScore;
    }

    public void setJobScore(int jobScore) {
        this.JobScore = jobScore;
    }

    public boolean isAppliedOrNot() {
        return AppliedOrNot;
    }

    public void setAppliedOrNot(boolean appliedOrNot) {
        this.AppliedOrNot = appliedOrNot;
    }


    public int getJobImageResourceId() {
        return this.ImageResourceId;
    }

    public void setJobImage(int resourceId) {
        this.ImageResourceId = resourceId;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }
}

