package com.example.actualassignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    static final int NOTESACTIVITY_REQUEST_CODE = 1;
    static final int JOBACTIVITY_REQUEST_CODE = 2;
    ListView listView;
    Button exitButton;
    JobListAdapter jobAdapter;
    ArrayList< Job > jobList;


    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent returnedIntent){
        super.onActivityResult(requestCode, resultCode, returnedIntent);
        Log.d("mainActivity", "Returned from other activity.");
        switch (requestCode){
            case (NOTESACTIVITY_REQUEST_CODE):
                Log.d("mainActivity", "Returned from NotesActivity.");
                if( resultCode == RESULT_OK )
                {
                    Log.d("mainActivity", "Returned from NotesActivity AND result is OK");
                    // Get data from returned intent.
                    String returnedNotesFromActivity = returnedIntent.getStringExtra("updatedNotes");
                    int returnedScoreFromActivity = returnedIntent.getIntExtra("updatedScore", 1 );
                    boolean returnedStatusFromActivity = returnedIntent.getBooleanExtra("updatedStatus", false );
                    int returnedPositionFromActivity = returnedIntent.getIntExtra("samePosition", 0 );

                    // Use that returned data.
                    jobList.get(returnedPositionFromActivity).setJobScore(returnedScoreFromActivity);
                    jobList.get(returnedPositionFromActivity).setJobNotes(returnedNotesFromActivity);
                    jobList.get(returnedPositionFromActivity).setAppliedOrNot(returnedStatusFromActivity);

                    // Test-code.
                    jobAdapter = new JobListAdapter(this, jobList);
                    listView.setAdapter(jobAdapter);


                }
                break;

        }
    }


    /*@Override
    public void onSaveInstanceState( Bundle savedInstanceState)
    {
        savedInstanceState.putInt("number of jobs", jobList.size());
        savedInstanceState.putInt("king score", jobList.get(1).getJobScore());

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState( Bundle savedInstanceState )
    {
        super.onRestoreInstanceState(savedInstanceState);
        int counter = savedInstanceState.getInt("numer of jobs", 1);
        int kingScore = savedInstanceState.getInt("king score", 1 );
        jobList = new ArrayList<>();
        readData(jobList);
        jobList.get(0).setJobScore(1);
    }*/






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        // Log
        Log.d("mainActivity", "onCreate: started");

        // Get button.
        exitButton = findViewById(R.id.exitButton);
        listView = (ListView) findViewById(R.id.listview);

        jobList = new ArrayList<>();
        readData(jobList);

        // Checks if there has been a rotation. If not, then the data is loaded in.
        if (savedInstanceState != null) {
            // Retrieve saved data, and save into jobList.
            for (int i = 0; i < jobList.size(); i++)
            {
                jobList.get(i).setJobScore(savedInstanceState.getInt(jobList.get(i).getDescription(), 1));
                jobList.get(i).setAppliedOrNot(savedInstanceState.getBoolean(jobList.get(i).getJobTitle(), false ));
                jobList.get(i).setJobNotes(savedInstanceState.getString(jobList.get(i).getLocation()));
            }
        }

        Log.d("mainActivity", "Added jobs to list");
        jobAdapter = new JobListAdapter(this, jobList);
        Log.d("mainActivity", "Done adapting.");

        listView.setAdapter(jobAdapter);
        Log.d("mainActivity", "Done setting adapter.");


        // Adding a click-sniffer.
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent startJobActivityIntent = new Intent(ListActivity.this, JobActivity.class);
                startJobActivityIntent.putExtra("position", position);
                Job tempJob = (Job) (listView.getItemAtPosition(position));
                startJobActivityIntent.putExtra("company", tempJob.getJobCompany());
                startJobActivityIntent.putExtra("title", tempJob.getJobTitle());
                startJobActivityIntent.putExtra("description", tempJob.getDescription());
                startJobActivityIntent.putExtra("score", tempJob.getJobScore());
                startJobActivityIntent.putExtra("notes", tempJob.getJobNotes());
                startJobActivityIntent.putExtra("appliedOrNot", tempJob.isAppliedOrNot());
                startJobActivityIntent.putExtra("jobLogo", tempJob.getImageResourceId());
                startJobActivityIntent.putExtra("jobLocation", tempJob.getLocation());


                startActivityForResult(startJobActivityIntent, JOBACTIVITY_REQUEST_CODE);

            }
        });


        // Adding a long-click-sniffer.
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Intent startNotesActivityIntent = new Intent(ListActivity.this, NotesActivity.class);
                startNotesActivityIntent.putExtra("position", position);
                Job tempJob = (Job) (listView.getItemAtPosition(position));
                startNotesActivityIntent.putExtra("company", tempJob.getJobCompany());
                startNotesActivityIntent.putExtra("title", tempJob.getJobTitle());
                startNotesActivityIntent.putExtra("description", tempJob.getDescription());
                startNotesActivityIntent.putExtra("score", tempJob.getJobScore());
                startNotesActivityIntent.putExtra("notes", tempJob.getJobNotes());
                startNotesActivityIntent.putExtra("appliedOrNot", tempJob.isAppliedOrNot());
                startNotesActivityIntent.putExtra("jobLogo", tempJob.getImageResourceId());
                startNotesActivityIntent.putExtra("jobLocation", tempJob.getLocation());


                startActivityForResult(startNotesActivityIntent, NOTESACTIVITY_REQUEST_CODE);


                return false;
            }
        });


        // Exit button click listener.
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }


    // Function for saving data on rotation.
    @Override
    public void onSaveInstanceState( Bundle savedInstanceState )
    {
        for( int i = 0; i < jobList.size(); i++ )
        {
            savedInstanceState.putString(jobList.get(i).getLocation(), jobList.get(i).getJobNotes());
            savedInstanceState.putInt(jobList.get(i).getDescription(), jobList.get(i).getJobScore());
            savedInstanceState.putBoolean(jobList.get(i).getJobTitle(), jobList.get(i).isAppliedOrNot());
        }

        super.onSaveInstanceState( savedInstanceState );
    }


    void readData( ArrayList< Job > joblist)
    {
        // Creates an input stream from the CSV file.
        InputStream is = getResources().openRawResource(R.raw.jobs);
        // Creates a buffered reader to read from the input-stream.
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8"))
        );

        // Initialization of string to save the data into.
        String line = "";
        try {
            while ( ( line = reader.readLine()) != null ){
                // Split data by commas.
                String[] tokens = line.split(";");

                // Read the data.
                Job jobsample = new Job(tokens[0], tokens[2], 1, false, tokens[3], tokens[1]);
                switch (tokens[0]) {
                    case "King":
                        jobsample.setJobImage(R.drawable.king);
                        break;
                    case "Pulsara":
                        jobsample.setJobImage(R.drawable.pulsara);
                        break;
                    case "Direct Supply, Inc.":
                        jobsample.setJobImage(R.drawable.directsupply);
                        break;
                    case "Pace Scheduler":
                        jobsample.setJobImage(R.drawable.pace);
                        break;
                    case "InnoGames GmbH":
                        jobsample.setJobImage(R.drawable.innogames);
                        break;
                    case "Avochato":
                        jobsample.setJobImage(R.drawable.avochato);
                        break;
                    case "Chatterbug":
                        jobsample.setJobImage(R.drawable.chatterbug);
                        break;
                    case "Favor Delivery":
                        jobsample.setJobImage(R.drawable.favor);
                        break;
                    case "Mindhive":
                        jobsample.setJobImage(R.drawable.mindhive);
                        break;
                    case "MojoTech":
                        jobsample.setJobImage(R.drawable.mojotech);
                        break;
                    case "Snapfish":
                        jobsample.setJobImage(R.drawable.snapfish);
                        break;
                }
                joblist.add( jobsample );
            }
        } catch (IOException e){
            Log.wtf("mainActivity", "error reading datafile on line " + line, e);
            e.printStackTrace();
        }
    }
}
