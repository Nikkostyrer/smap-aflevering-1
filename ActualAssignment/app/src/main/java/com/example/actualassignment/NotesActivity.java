package com.example.actualassignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class NotesActivity extends AppCompatActivity {

    ImageView theJobImageId;
    TextView theJobScore;
    TextView theJobLocation;
    TextView theJobCompany;
    TextView theJobTitle;
    TextView theJobDescription;
    EditText theNotes;
    TextView theIsItApplied;
    boolean theStatus;
    CheckBox applianceBox;
    Button cancelButtonNotes;
    Button okButtonNotes;

    int receivedPosition = 0;

    SeekBar scoreBar;

    int jobScoreNotes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        Intent receivedIntent = getIntent();

        theJobScore = findViewById(R.id.scoreNotesXML);
        theJobTitle = findViewById(R.id.titleNotesXML);
        theJobCompany = findViewById(R.id.companyNotesXML);
        theNotes = findViewById(R.id.notesNotesXML);
        theIsItApplied = findViewById(R.id.appliedOrNotNotesXML);
        cancelButtonNotes = findViewById(R.id.cancelButtonNotesXML);
        okButtonNotes = findViewById(R.id.okButtonNotesXML);
        applianceBox = findViewById(R.id.applianceCheckBoxNotesXML);

        receivedPosition = receivedIntent.getIntExtra("position", 0 );

        scoreBar = findViewById(R.id.seekBar);

        theJobCompany.setText(receivedIntent.getStringExtra("company"));
        theJobTitle.setText(receivedIntent.getStringExtra("title"));

        theNotes.setMovementMethod(new ScrollingMovementMethod());
        jobScoreNotes = receivedIntent.getIntExtra("score", 1 );

        if( savedInstanceState != null )
        {
            theNotes.setText(savedInstanceState.getString("notes"));
            theJobScore.setText(String.valueOf(savedInstanceState.getInt("score")));
        }
        else
        {
            theNotes.setText(receivedIntent.getStringExtra("notes"));
            theJobScore.setText(String.valueOf(jobScoreNotes));
        }

        theStatus = receivedIntent.getBooleanExtra("appliedOrNot", false );
        if( savedInstanceState != null )
        {
            theStatus = savedInstanceState.getBoolean("status");
        }
        if (theStatus)
        {
            theIsItApplied.setText("Applied.");
        }
        else
        {
            theIsItApplied.setText("Not applied.");
        }

        applianceBox.setChecked(theStatus);




        // Doing some seekbar coding.
        scoreBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            //int progress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressVal, boolean fromUser) {
                //progress = progressVal;
                jobScoreNotes = progressVal;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                theJobScore.setText(String.valueOf(jobScoreNotes));
            }
        });
        // Seekbar code ends here.




        // Cancel button code.
        cancelButtonNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Just finishes without returning any info.
                finish();
            }
        });
        // Cancel button code ends here.




        // OK button code.
        okButtonNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Must return info:
                    //- notes. as string.
                    //- score. as int.
                    //- appliedOrNot. as boolean.
                String returnedNotes = theNotes.getText().toString();

                Intent resultIntent = new Intent();

                resultIntent.putExtra("updatedNotes", returnedNotes);
                resultIntent.putExtra("updatedScore", jobScoreNotes);
                resultIntent.putExtra("updatedStatus", theStatus);
                resultIntent.putExtra("samePosition", receivedPosition);

                setResult(ListActivity.RESULT_OK, resultIntent);
                finish();
            }
        });
        // OK button code ends here.


    }

    public void onCheckBoxClick(View view)
    {
        boolean isBoxChecked = ((CheckBox)view).isChecked();

        switch (view.getId())
        {
            case R.id.applianceCheckBoxNotesXML:
                if (isBoxChecked)
                {
                    theStatus = true;
                    theIsItApplied.setText("Applied.");
                }
                else
                {
                    theStatus = false;
                    theIsItApplied.setText("Not applied.");
                }
        }
    }

    @Override
    public void onSaveInstanceState( Bundle savedInstanceState )
    {
        savedInstanceState.putBoolean("status", theStatus );
        savedInstanceState.putInt("score", jobScoreNotes );
        savedInstanceState.putString("notes", theNotes.getText().toString());

        super.onSaveInstanceState(savedInstanceState);
    }
}
