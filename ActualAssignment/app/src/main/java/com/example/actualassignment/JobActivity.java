package com.example.actualassignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class JobActivity extends AppCompatActivity {

    Button okButton;
    ImageView theJobImageId;
    TextView theJobScore;
    TextView theJobLocation;
    TextView theJobCompany;
    TextView theJobTitle;
    TextView theJobDescription;
    TextView theNotes;
    TextView theIsItApplied;
    boolean theStatus;
    int scorez = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);

        Intent receivedIntent = getIntent();

        okButton = findViewById(R.id.okButtonJobXML);
        theJobImageId = findViewById(R.id.logoJobXML);
        theJobScore = findViewById(R.id.companyScoreJobXML);
        theJobLocation = findViewById(R.id.companyLocationJobXML);
        theJobCompany = findViewById(R.id.companyNameJobXML);
        theJobTitle = findViewById(R.id.jobTitleJobXML);
        theJobDescription = findViewById(R.id.jobDescriptionJobXML);
        theNotes = findViewById(R.id.jobNotesJobXML);
        theIsItApplied = findViewById(R.id.jobStatusJobXML);


        theJobImageId.setImageResource(receivedIntent.getIntExtra("jobLogo", 0));
        if( savedInstanceState != null )
        {
            theJobScore.setText(String.valueOf(savedInstanceState.getInt("score")));
            theNotes.setText(savedInstanceState.getString("notes"));
        }
        else
        {
            theJobScore.setText(String.valueOf(receivedIntent.getIntExtra("score", 1)));
            theNotes.setText(receivedIntent.getStringExtra("notes"));
        }
        scorez = receivedIntent.getIntExtra("score", 1 );
        theJobLocation.setText(receivedIntent.getStringExtra("jobLocation"));
        theJobCompany.setText(receivedIntent.getStringExtra("company"));
        theJobTitle.setText(receivedIntent.getStringExtra("title"));
        theJobDescription.setText(receivedIntent.getStringExtra("description"));

        theStatus = receivedIntent.getBooleanExtra("appliedOrNot", false );

        if (savedInstanceState != null )
        {
            theStatus = savedInstanceState.getBoolean("appliance");
        }

        if (theStatus)
        {
            theIsItApplied.setText("Applied");
        }
        else
        {
            theIsItApplied.setText("Not applied");
        }

        theJobDescription.setMovementMethod(new ScrollingMovementMethod());

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }


    @Override
    public void onSaveInstanceState( Bundle savedInstanceState )
    {
        savedInstanceState.putString("notes", theNotes.getText().toString());
        savedInstanceState.putInt("score", scorez);
        savedInstanceState.putBoolean("appliance", theStatus );

        super.onSaveInstanceState( savedInstanceState );
    }
}
